use std::fs::File;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashSet;

#[derive(Eq, Hash, PartialEq, Clone, Copy)]
enum Movement {
    Up,
    Right,
    Down,
    Left
}

type Grid = Vec<Vec<char>>;

fn main() {
    let input_args: Vec<String> = env::args().collect();
    let file_content = read_file(&input_args[1]);
    let grid: Grid = file_content.into_iter().map(|x| x.chars().collect()).collect();
    let distinct_positions = move_along(&grid);
    match &distinct_positions {
        Some(p) => println!("Solution 1 is: {}", p.len()),
        None => panic!("Failed to find solution 1.")
    }
    let mut looping_obstacles = 0;
    let poslist = distinct_positions.unwrap();
    for this_pos in &poslist {
        let mut new_grid = grid.clone();
        if new_grid[this_pos.0][this_pos.1] != '^' {
            new_grid[this_pos.0][this_pos.1] = '#';
            let check_loop = move_along(&new_grid);
            looping_obstacles += match check_loop {
                Some(_) => 0,
                None => 1
            };
        }
    }
    println!("Solution 2 is: {}", looping_obstacles);
}

fn read_file(file_name: &String) -> Vec<String> {
    let file_handle = File::open(file_name).unwrap();
    let file_reader = BufReader::new(file_handle);
    let file_content: Vec<String> = file_reader.lines().map(|x| x.unwrap()).collect();
    file_content
}

fn move_along(grid: &Grid) -> Option<HashSet<(usize, usize)>> {
    let mut mov = Movement::Up;
    let mut pos = find_start_pos(&grid);
    let mut visited_positions = HashSet::new();
    let mut visited_positions_dir = HashSet::new();
    visited_positions.insert(pos);
    visited_positions_dir.insert((pos.0, pos.1, mov));
    // assuming that the grid is rectangular
    let lower_bound = 0;
    let upper_bound = grid.len();
    loop {
        if pos.0 == 0 && matches!(mov, Movement::Up){
            break
        } else if pos.0 == upper_bound - 1 && matches!(mov, Movement::Down) {
            break
        } else if pos.1 == lower_bound && matches!(mov, Movement::Left) {
            break
        } else if pos.1 == upper_bound - 1 && matches!(mov, Movement::Right) {
            break
        }
        let next_pos = do_move(&pos, &mov);
        if grid[next_pos.0][next_pos.1] == '#' {
            mov = match mov {
                Movement::Up => Movement::Right,
                Movement::Right => Movement::Down,
                Movement::Down => Movement::Left,
                Movement::Left => Movement::Up
            }
        } else {
            visited_positions.insert(next_pos);
            if !visited_positions_dir.insert((next_pos.0, next_pos.1, mov)) {
                return None
            }
            pos = next_pos;
        }
    }
    Some(visited_positions)
}

fn find_start_pos(grid: &Grid) -> (usize, usize) {
    for y in 0..grid.len() {
        for x in 0..grid[0].len() {
            if grid[y][x] == '^' {
                return (y, x)
            }
        }
    }
    (0usize, 0usize)
}

fn do_move(pos: &(usize, usize), direction: &Movement) -> (usize, usize) {
    match direction {
        Movement::Up => (pos.0 - 1, pos.1),
        Movement::Right => (pos.0, pos.1 + 1),
        Movement::Down => (pos.0 + 1, pos.1),
        Movement::Left => (pos.0, pos.1 - 1)
    }
}