use std::fs::File;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;
use regex::Regex;

fn main() {
    let input_args: Vec<String> = env::args().collect();
    let file_content = read_file(&input_args[1]);
    let joined_content = file_content.join(" ");
    let results = extract(&joined_content);
    println!("Solution 1 is: {}", get_solution(&results[..]));

    let do_parts: Vec<&str> = joined_content.split_inclusive("do()").collect();
    let mut do_parts_filtered: Vec<&str> = Vec::new(); 
    for part in do_parts {
        do_parts_filtered.push(part.split("don't()").next().unwrap());
    }
    let only_do = do_parts_filtered.join(" ");
    let results2 = extract(&only_do);
    println!("Solution 2 is: {}", get_solution(&results2[..]));
}

fn read_file(file_name: &String) -> Vec<String> {
    let file_handle = File::open(file_name).unwrap();
    let file_reader = BufReader::new(file_handle);
    let file_content: Vec<String> = file_reader.lines().map(|x| x.unwrap()).collect();
    file_content
}

fn extract(this_string: &str) -> Vec<(i32, i32)> {
    let re = Regex::new(r"mul\((?<x0>\d{1,3}),(?<x1>\d{1,3})\)").unwrap();
    let results: Vec<(i32, i32)> = re
        .captures_iter(&this_string)
        .map(|m| (
            m.name("x0").unwrap().as_str().parse::<i32>().unwrap(),
            m.name("x1").unwrap().as_str().parse::<i32>().unwrap()
        ))
        .collect();
    results
}

fn get_solution(results: &[(i32, i32)]) -> i32 {
    let products: Vec<i32> = results.iter().map(|x| x.0 * x.1).collect();
    products.iter().sum()
}