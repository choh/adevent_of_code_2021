use std::fs::File;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::{HashSet, HashMap};

type Grid = Vec<Vec<char>>;
type Positions = HashMap<char, Vec<(usize, usize)>>;

fn main() {
    let input_args: Vec<String> = env::args().collect();
    let file_content = read_file(&input_args[1]);
    let grid: Grid = file_content.into_iter().map(|x| x.chars().collect()).collect();
    let positions: Positions = extract_coordinates(&grid);

    // part 1
    let mut antinodes: HashSet<(usize, usize)> = HashSet::new();
    for (_, pos) in &positions {
        for i in 0..pos.len() {
            for j in 0..pos.len() {
                if j == i {
                    continue;
                }
                match find_antinode(&pos[i], &pos[j], grid.len() as i32) {
                    Some(x) => antinodes.insert(x),
                    None => false,
                };
            }
        }
    }
    println!("Solution 1 is {}", antinodes.len());

    // part 2
    let mut ext_antinodes: HashSet<(usize, usize)> = HashSet::new();
    for (_, pos) in &positions {
        for i in 0..pos.len() {
            for j in 0..pos.len() {
                if j == i {
                    continue;
                }
                let res = find_extended_antinodes(&pos[i], &pos[j], grid.len() as i32);
                for item in res {
                    ext_antinodes.insert(item);
                }
            }
        }
    }
    println!("Solution 2 is {}", ext_antinodes.len());
}

fn read_file(file_name: &String) -> Vec<String> {
    let file_handle = File::open(file_name).unwrap();
    let file_reader = BufReader::new(file_handle);
    let file_content: Vec<String> = file_reader.lines().map(|x| x.unwrap()).collect();
    file_content
}

fn extract_coordinates(grid: &Grid) -> Positions {
    let mut pos: Positions = HashMap::new();
    for y in 0..grid.len() {
        for x in 0..grid[y].len() {
            if grid[y][x] != '.' {
                pos.entry(grid[y][x]).and_modify(|v| v.push((y, x))).or_insert(vec![(y, x)]);
            }
        }
    }
    pos
}

fn find_antinode(p0: &(usize, usize), p1: &(usize, usize), max: i32) -> Option<(usize, usize)> {
    let ymov = p1.0 as i32 - p0.0 as i32;
    let xmov = p1.1 as i32 - p0.1 as i32;
    let chk = (p0.0 as i32 + ymov * 2, p0.1 as i32 + xmov * 2);
    if chk.0 >= 0 && chk.1 >= 0 && chk.0 < max && chk.1 < max {
        return Some((chk.0 as usize, chk.1 as usize));
    }
    None
}

fn find_extended_antinodes(p0: &(usize, usize), p1: &(usize, usize), max: i32) -> Vec<(usize, usize)> {
    let mut antinodes: Vec<(usize, usize)> = Vec::new();
    let ymov = p1.0 as i32 - p0.0 as i32;
    let xmov = p1.1 as i32 - p0.1 as i32;
    let mut f: i32 = 1;
    loop {
        let chk = (p0.0 as i32 + ymov * f, p0.1 as i32 + xmov * f);
        if chk.0 >= 0 && chk.1 >= 0 && chk.0 < max && chk.1 < max {
            antinodes.push((chk.0 as usize, chk.1 as usize));
            f += 1;
        } else {
            break;
        }
        
    }
    antinodes
}