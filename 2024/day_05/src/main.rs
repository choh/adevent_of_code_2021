use std::fs::File;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashMap;

type Rules = HashMap<u32, Vec<u32>>;

fn main() {
    let input_args: Vec<String> = env::args().collect();
    let file_content = read_file(&input_args[1]);
    let mut rules: Rules = HashMap::new();
    let mut pages: Vec<Vec<u32>> = Vec::new();
    let mut first = true;
    for line in &file_content {
        if line.len() < 2 {
            first = false;
            continue
        }
        if first {
            let rule: (u32, u32) = handle_ruleset(&line);
            rules.entry(rule.0).and_modify(|v| v.push(rule.1)).or_insert(vec![rule.1]);
        } else {
            let this_page_list = handle_pages(&line);
            pages.push(this_page_list);
        }
    }
    let mut middle_numbers_sum: u32 = 0;
    let mut sorted_middle_numbers_sum: u32 = 0;
    for page_list in &pages {
        let is_correct = check_rules(&page_list[..], &rules);
        if is_correct.0 {
            middle_numbers_sum += page_list[(page_list.len() - 1) / 2]
        } else {
            let sorted_data = sort_pages(&page_list[..], &rules);
            sorted_middle_numbers_sum += sorted_data[(sorted_data.len() - 1) / 2]
        }
    }
    println!("Solution 1 is: {}", middle_numbers_sum);
    println!("Solution 2 is: {}", sorted_middle_numbers_sum);
}

fn read_file(file_name: &String) -> Vec<String> {
    let file_handle = File::open(file_name).unwrap();
    let file_reader = BufReader::new(file_handle);
    let file_content: Vec<String> = file_reader.lines().map(|x| x.unwrap()).collect();
    file_content
}

fn handle_ruleset(line: &String) -> (u32, u32) {
    let parts: Vec<&str> = line.split('|').collect();
    let nums: Vec<u32> = parts.iter().map(|x| x.parse::<u32>().unwrap()).collect();
    (nums[0], nums[1])
}

fn handle_pages(line: &String) -> Vec<u32> {
    let parts: Vec<&str> = line.split(',').collect();
    let nums: Vec<u32> = parts.iter().map(|x| x.parse::<u32>().unwrap()).collect();
    nums
}

fn check_rules(data: &[u32], rules: &Rules) -> (bool, usize) {
    for i in 1..data.len() {
        for j in 0..i {
            match rules.get(&data[i]) {
                Some(v) => {
                    if v.contains(&data[j]) {
                        return (false, i)
                    }
                },
                None => (),
            }
        }
    }
    (true, 0usize)
}

fn sort_pages(data: &[u32], rules: &Rules) -> Vec<u32> {
    let mut this_data = data.to_vec();
    loop {
        let chk_result = check_rules(&this_data[..], rules);
        if chk_result.0 {
            return this_data.to_vec()
        }
        let offending_value = this_data.remove(chk_result.1);
        this_data.insert(chk_result.1-1, offending_value);
    }
}