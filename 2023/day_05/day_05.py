#! /usr/bin/env python3
import math
import re
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    seeds = parse_seeds(input_lines[0])
    conversion = parse_almanac(input_lines[1:])
    locations = []
    for seed in seeds:
        locations.append(seed_to_location(seed, "seed", conversion))
    print(f"The solution for part 1 is {min(locations)}")
    seeds_2 = []
    range_2 = []
    for i, s in enumerate(seeds):
        if i % 2 == 0:
            seeds_2.append(s)
        else:
            range_2.append(s)
    min_location = math.inf

    for j, s in enumerate(seeds_2):
        for i in range(s, s + range_2[j]):
            print(f"{i} | {s + range_2[j]}\r", end=" ")
            this_loc = seed_to_location(i, "seed", conversion)
            if this_loc < min_location:
                min_location = this_loc
    print(f"The solution for part 2 is {min_location}")

def parse_seeds(seed_line):
    seed_line = seed_line.strip().replace("seeds: ", "")
    seeds = [int(x) for x in seed_line.split(" ")]
    return seeds

def parse_almanac(almanac_lines):
    conversion = {}
    num_re = re.compile(r"\d")
    for line in almanac_lines:
        if len(line.strip()) == 0:
            continue
        elif "map" in line:
            conversion_from, _, conversion_to = line \
                .strip() \
                .replace(" map:", "") \
                .split("-") 
            conversion[conversion_from] = { "to": conversion_to }
        elif num_re.match(line):
            nums = line.strip().split(" ")
            to_num, from_num, length = (int(x) for x in nums)
            mapping = {
                "from_start": from_num,
                "from_end": from_num + length - 1,
                "to_start": to_num,
                "to_end": to_num + length - 1,
                "length": length
            }
            if "mapping" in conversion[conversion_from]:
                conversion[conversion_from]["mapping"].append(mapping)
            else:
                conversion[conversion_from]["mapping"] = [mapping]
    return conversion

def seed_to_location(seed, state, almanac):
    if state == "location":
        return seed
    for conv_map in almanac[state]["mapping"]:
        if seed >= conv_map["from_start"] and seed <= conv_map["from_end"]:
            offset = conv_map["to_start"] - conv_map["from_start"]
            seed = seed + offset
            break
    return seed_to_location(seed, almanac[state]["to"], almanac)

if __name__ == "__main__":
    main()