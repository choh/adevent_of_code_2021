#! /usr/bin/env python3
from pathlib import Path
from copy import deepcopy
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [list(line.rstrip()) for line in f]
    tilted_grid = tilt_vert(input_lines)
    solution_1 = calculate_weight(tilted_grid)
    print(f"The solution for part 1 is {solution_1}")
    
    target = 1000000000 - 1
    first_target = 200
    this_grid = deepcopy(input_lines)
    grid_weights = []
    for _ in range(first_target):
        this_grid = tilt_cycle(this_grid)
        grid_weights.append(calculate_weight(this_grid))
    for i, _ in enumerate(grid_weights):
        cycle_len = find_repetition(grid_weights[i:])
        if cycle_len != -1:
            cycle_info = {
                "start": i, 
                "length": cycle_len, 
                "cycle": grid_weights[i:i+cycle_len]
            }
            break
    useless_loops = target // cycle_info["length"]
    startpos = useless_loops * cycle_info["length"] + cycle_info["start"]
    needed_iterations = target - startpos
    offset = 0
    while needed_iterations < 0:
        offset += 1
        startpos = (useless_loops - offset) * cycle_info["length"] + cycle_info["start"]
        needed_iterations = target - startpos
    solution_2 = cycle_info["cycle"][needed_iterations]
    print(f"The solution for part 2 is {solution_2}")

def find_repetition(value_list):
    max_sequence_length = int(len(value_list) / 2) + 1
    for x in range(2, max_sequence_length):
        if value_list[0:x] == value_list[x:2*x] :
            return x
    return -1

def tilt_cycle(input_grid):
    north = tilt_vert(input_grid)
    west = tilt_west(north)
    south = tilt_vert(list(reversed(west)), south=True)
    east = tilt_east(south)
    return east

def tilt_vert(input_grid, south=False):
    grid = deepcopy(input_grid)
    for y, grid_row in enumerate(grid):
        for x, field in enumerate(grid_row):
            if field == "O":
                use_col = get_col(grid, x)
                use_col = use_col[:y]
                new_y = y
                for item in reversed(use_col):
                    if item in "#O":
                        break
                    new_y -= 1
                grid[y][x] = "."
                grid[new_y][x] = "O"
    if south:
        grid = list(reversed(grid))
    return grid

def tilt_west(input_grid):
    grid = deepcopy(input_grid)
    for y, grid_row in enumerate(grid):
        for x, field in enumerate(grid_row):
            if field == "O":
                use_row = grid_row[:x]
                new_x = x
                for item in reversed(use_row):
                    if item in "#O":
                        break
                    new_x -= 1
                grid[y][x] = "."
                grid[y][new_x] = "O"
    return grid

def tilt_east(input_grid):
    grid = deepcopy(input_grid)
    for y, grid_row in enumerate(grid):
        for rx, field in enumerate(reversed(grid_row)):
            if field == "O":
                x = len(grid_row) - rx - 1
                use_row = grid_row[x+1:]
                new_x = x
                for item in use_row:
                    if item in "#O":
                        break
                    new_x += 1
                grid[y][x] = "."
                grid[y][new_x] = "O"
    return grid

def calculate_weight(grid):
    weight = 0
    for i, row in enumerate(grid):
        stones_count = row.count("O")
        row_weight = stones_count * (len(grid) - i)
        weight += row_weight
    return weight

def get_col(grid, x):
    return [row[x] for row in grid]

def print_grid(grid):
    for row in grid:
        print(row)

if __name__ == "__main__":
    main()
    