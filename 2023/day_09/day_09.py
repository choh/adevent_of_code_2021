#! /usr/bin/env python3
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    input_lines = [line.split() for line in input_lines]
    input_nums = []
    for line in input_lines:
        input_nums.append([int(x) for x in line])
    extended_values = [interpolate(sequence) for sequence in input_nums]
    solution_1 = sum(extended_values)
    print(f"The solution for part 1 is {solution_1}")
    prepended_values = [interpolate(list(reversed(sequence))) for sequence in input_nums]
    solution_2 = sum(prepended_values)
    print(f"The solution for part 2 is {solution_2}")

def interpolate(sequence):
    child_seq = [sequence]
    while True:
        # index i is offset by 1 here compared to sequence
        # as we start enumerating from sequence[1]
        sequence = [item - sequence[i] for i, item in enumerate(sequence[1:])]
        child_seq.append(sequence)
        if sequence[0] == 0 and len(set(sequence)) == 1:
            break
    child_seq = list(reversed(child_seq))
    for i, item in enumerate(child_seq):
        if i + 1 == len(child_seq):
            break
        new_val = item[-1] + child_seq[i+1][-1]
        child_seq[i+1].append(new_val)
    return child_seq[-1][-1]


if __name__ == "__main__":
    main()