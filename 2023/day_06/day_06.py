#! /usr/bin/env python3
import math
import re
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    race_nums = [re.sub("[A-Za-z:]", "", line).strip() for line in input_lines]
    race_nums = [re.split("\s+", race) for race in race_nums]
    races = []
    for i, item in enumerate(race_nums[0]):
        this_race = {
            "time": int(item),
            "distance": int(race_nums[1][i])
        }
        races.append(this_race)
    record_beating = [get_record(r) for r in races]
    solution_1 = math.prod(record_beating)
    print(f"The solution for part 1 is {solution_1}")

    final_race = {
        "time":     int("".join([str(x["time"]) for x in races])),
        "distance": int("".join([str(x["distance"]) for x in races]))
    }
    solution_2 = get_record(final_race)
    print(f"The solution for part 2 is {solution_2}")

def get_record(race):
    for s in range(0, race["time"]):
        distance_travelled = s * (race["time"] - s)
        if distance_travelled > race["distance"]:
            max_time = race["time"] - s
            ways_to_win = max_time - s + 1
            break
    else:
        ways_to_win = 0
    return ways_to_win


if __name__ == "__main__":
    main()