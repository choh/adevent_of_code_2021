#! /usr/bin/env python3
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    games = []
    for line in input_lines:
        hand, bid = line.strip().split(" ")
        games.append({ "hand": hand, "bid": int(bid) })
    scored_games = [score_hand(game) for game in games]
    ranked_games = sorted(scored_games, key = lambda x: \
        (x["score"], x["cscore"][0], x["cscore"][1], x["cscore"][2],
         x["cscore"][3], x["cscore"][4]))
    total_winnings = []
    for i, item in enumerate(ranked_games):
        winning = (i + 1) * item["bid"]
        total_winnings.append(winning)
    solution_1 = sum(total_winnings)
    print(f"The solution for part 1 is {solution_1}")

def score_hand(hand):
    hand_set = set(hand["hand"])
    card_counts = count_cards(hand["hand"])
    nums = list(card_counts.values())
    if len(hand_set) == 5:
        hand_score = 0
    elif len(hand_set) == 4:
        hand_score = 1
    elif len(hand_set) == 3:
        if 3 in nums:
            hand_score = 3
        else:
            hand_score = 2
    elif len(hand_set) == 2:
        if 4 in nums:
            hand_score = 5
        else:
            hand_score = 4
    else:
        hand_score = 6
    hand["score"] = hand_score
    hand["cscore"] = translate_hand(hand["hand"])
    return hand

def count_cards(hand_str):
    counts = {}
    for letter in hand_str:
        if letter in counts:
            counts[letter] += 1
        else:
            counts[letter] = 1
    return counts

def translate_hand(hand_str):
    card_scores = []
    for letter in hand_str:
        try:
            score = int(letter)
            card_scores.append(score)
        except ValueError:
            if letter == "T":
                card_scores.append(10)
            elif letter == "J":
                card_scores.append(11)
            elif letter == "Q":
                card_scores.append(12)
            elif letter == "K":
                card_scores.append(13)
            elif letter == "A":
                card_scores.append(14)       
    return card_scores     

if __name__ == "__main__":
    main()