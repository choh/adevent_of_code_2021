#! /usr/bin/env python3
import math
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    game_results = [extract_game_info(line) for line in input_lines]
    solution_1 = sum_of_possible_game_ids(game_results)
    print(f"The solution for part 1 is {solution_1}")
    game_minimuns = [minimums_per_game(game) for game in game_results]
    solution_2 = sum_of_game_powers(game_minimuns)
    print(f"The solution for part 2 is {solution_2}")


def extract_game_info(game_line):
    _, game_data = game_line.split(":")
    game_data = game_data.replace(",", "")
    game_data = game_data.split(";")
    game_data = [x.strip() for x in game_data]
    #breakpoint()
    game_list = []
    for trial in game_data:
        tokens = trial.split(" ")
        trial_data = {}
        for i in range(0, len(tokens), 2):
            trial_data[tokens[i + 1]] = int(tokens[i])
        game_list.append(trial_data)
    return game_list

def is_possible(to_check, red, green, blue):
    if "red" in to_check:
        if to_check["red"] > red:
            return False
    if "green" in to_check:
        if to_check["green"] > green:
            return False
    if "blue" in to_check:
        if to_check["blue"] > blue:
            return False
    return True

def sum_of_possible_game_ids(result_list):
    solution_1 = 0
    for num, game in enumerate(result_list):
        for trial in game:
            if not is_possible(trial, 12, 13, 14):
                break
        else:
            solution_1 += num + 1
    return solution_1

def minimums_per_game(game_result):
    minimums = {
        "red": 0,
        "green": 0,
        "blue": 0
    }
    for trial in game_result:
        if "red" in trial:
            if trial["red"] > minimums["red"]:
                minimums["red"] = trial["red"]
        if "green" in trial:
            if trial["green"] > minimums["green"]:
                minimums["green"] = trial["green"]
        if "blue" in trial:
            if trial["blue"] > minimums["blue"]:
                minimums["blue"] = trial["blue"]
    return minimums

def sum_of_game_powers(min_list):
    power_sum = 0
    for this_min in min_list:
        values = [v for v in this_min.values()]
        power_sum += math.prod(values)
    return power_sum

if __name__ == "__main__":
    main()