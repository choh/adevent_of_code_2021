#! /usr/bin/env python3
from pathlib import Path
import sys

class LensBoxes:
    def __init__(self):
        self.boxes = [[] for _ in range(256)]

    def handle_lens(self, instruction):
        if instruction[-1] == "-":
            lens_id = instruction.split("-")[0]
            this_lens = {
                "id": lens_id,
                "box": hash_string(lens_id),
                "idx": None,
                "focal_length": None,
                "instruction": "remove"
            }
        else:
            lens_id, focal_length = instruction.split("=")
            this_lens = {
                "id": lens_id,
                "box": hash_string(lens_id),
                "idx": None,
                "focal_length": int(focal_length),
                "instruction": "insert"
            }
        this_lens["idx"] = self.find_lens(this_lens)
        if this_lens["instruction"] == "insert":
            self.insert_lens(this_lens)
        elif this_lens["instruction"] == "remove":
            self.remove_lens(this_lens)

    def find_lens(self, lens):
        for i, present_lens in enumerate(self.boxes[lens["box"]]):
            if present_lens["id"] == lens["id"]:
                return i
        return -1
    
    def insert_lens(self, lens):
        if lens["idx"] == -1:
            self.boxes[lens["box"]].append(lens)
        else:
            self.boxes[lens["box"]][lens["idx"]] = lens

    def remove_lens(self, lens):
        if lens["idx"] != -1:
            del self.boxes[lens["box"]][lens["idx"]]

    def total_power(self):
        total = 0
        for i, box in enumerate(self.boxes):
            for j, lens in enumerate(box):
                total += (i + 1) * (j + 1) * lens["focal_length"]
        return total


def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    tokens = []
    for line in input_lines:
        these_items = line.split(",")
        for item in these_items:
            tokens.append(item)
    hash_values = []
    for tok in tokens:
        hash_values.append(hash_string(tok))
    print(f"The solution for part 1 is {sum(hash_values)}")

    lensbox = LensBoxes()
    for tok in tokens:
        lensbox.handle_lens(tok)
    print(f"The solution for part 2 is {lensbox.total_power()}")


def hash_string(input_string):
    current_value = 0
    for char in input_string:
        current_value += ord(char)
        current_value *= 17
        current_value = current_value % 256
    return current_value

if __name__ == "__main__":
    main()
    