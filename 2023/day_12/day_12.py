#! /usr/bin/env python3
from pathlib import Path
import functools
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    springs = [parse_springs(s) for s in input_lines]
    solution_1 = 0
    for sp in springs:
        solution_1 += find_configurations(sp["spring"], sp["groups"])
    print(f"The solution for part 1 is {solution_1}")

    long_springs = [expand(s) for s in springs]
    solution_2 = 0
    for sp in long_springs:
        solution_2 += find_configurations(sp["spring"], sp["groups"])
    print(f"The solution for part 2 is {solution_2}")

def parse_springs(spring_row):
    springs, group = spring_row.split(" ")
    group_num = [int(x) for x in group.split(",")]
    return {
        "spring": springs,
        "groups": tuple(group_num)
    }

def expand(spring_data):
    return {
        "spring": ((spring_data["spring"] + "?") * 4) + spring_data["spring"],
        "groups": tuple(spring_data["groups"] * 5)
    }

@functools.cache
def find_configurations(spring, groups):
    if not groups:
        if "#" not in spring:
            return 1
        else:
            return 0

    if not spring:
        return 0

    next_char = spring[0]
    next_group = groups[0]

    def hash_sign():
        this_group = spring[:next_group]
        this_group = this_group.replace("?", "#")

        # see if desired size fits into group
        if this_group != "#" * next_group:
            return 0

        # see if it is last group (case if one remaining)
        if len(spring) == next_group:
            if len(groups) == 1:
                return 1
            else:
                return 0

        # see if separator can follow group
        if spring[next_group] in "?.":
            return find_configurations(spring[next_group+1: ], groups[1:])

        return 0

    def dot():
        return find_configurations(spring[1:], groups)

    if next_char == "#":
        out = hash_sign()

    if next_char == ".":
        out = dot()

    if next_char == "?":
        out = hash_sign() + dot()

    return out


if __name__ == "__main__":
    main()
