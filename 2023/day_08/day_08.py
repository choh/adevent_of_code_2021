#! /usr/bin/env python3
import re
from pathlib import Path
import sys

class Graph:
    def __init__(self):
        self.nodes = []

    def add_node(self, node):
        self.nodes.append(node)

    def get_node(self, name):
        matches = [x for x in self.nodes if x.name == name]
        return matches[0]


class Node:
    def __init__(self, name):
        self.name = name
        self.left = None
        self.right = None

    def add_child(self, left=None, right=None):
        self.left = left
        self.right = right

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    directions = input_lines[0]
    graph = Graph()
    for line in input_lines[2:]:
        clean_line = re.sub(r"[=(),]", "", line)
        clean_line = re.sub(r"\s+", " ", clean_line)
        name, left, right = clean_line.split(" ")
        this_node = Node(name)
        this_node.add_child(left, right)
        graph.add_node(this_node)
    solution_1 = find_steps(graph, "AAA", "ZZZ", directions)
    print(f"The solution for part 1 is {solution_1}")

    starting_positions = [x.name for x in graph.nodes if re.match(".+A$", x.name)]
    steps_2 = [find_steps(graph, spos, r".+Z$", directions) for spos in starting_positions]
    divisors = [find_divisors(num) for num in steps_2]
    cd_set = set.intersection(*map(set, divisors))
    gcd = max(cd_set)
    current_lcm = steps_2[0]
    for item in steps_2[1:]:
        current_lcm = lcm(current_lcm, item, gcd)
    print(f"The solution for part 2 is {int(current_lcm)}")


def find_steps(graph, from_node, to_pattern, moves):
    current_node = from_node
    move_step = 0
    step_count = 0
    while not re.match(to_pattern, current_node):
        this_node = graph.get_node(current_node)
        if moves[move_step] == "L":
            current_node = this_node.left
        else:
            current_node = this_node.right
        step_count += 1
        move_step += 1
        if move_step == len(moves):
            move_step = 0
    return step_count

def lcm(x, y, gcd):
    return x * y / gcd

def find_divisors(num):
    if num % 2 == 0:
        upper_limit = int(num / 2)
    else:
        upper_limit = int((num + 1) / 2)
    divs = []
    for i in range(1, upper_limit + 1):
        if num % i == 0:
            divs.append(i)
    return divs

if __name__ == "__main__":
    main()