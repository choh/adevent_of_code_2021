#! /usr/bin/env python3
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    games = [parse_game(line) for line in input_lines]
    wins_per_game = [count_wins(game) for game in games]
    solution_1 = sum([2 ** (x - 1) for x in wins_per_game if x > 0])
    print(f"The solution for part 1 is {solution_1}")
    number_of_cards = [1] * len(wins_per_game)
    for i, wins in enumerate(wins_per_game):
        for _ in range(0, number_of_cards[i]):
            for k in range(1, wins + 1):
                number_of_cards[i + k] += 1
    solution_2 = sum(number_of_cards)
    print(f"The solution for part 2 is {solution_2}")



def parse_game(game_line):
    card_no, game_info = game_line.split(":")
    card_no = int(card_no.replace("Card ", ""))
    winning, draw = game_info.split("|")
    winning_numbers_raw = winning.strip().split(" ")
    winning_numbers = [int(x.strip()) for x in winning_numbers_raw if len(x) > 0]
    draw_numbers_raw = draw.strip().split(" ")

    draw_numbers = [int(x.strip()) for x in draw_numbers_raw if len(x) > 0]
    return {
        "game": card_no,
        "winning": winning_numbers,
        "draw": draw_numbers
    }

def count_wins(this_game):
    wins = 0
    for num in this_game["draw"]:
        if num in this_game["winning"]:
            wins += 1
    return wins

if __name__ == "__main__":
    main()