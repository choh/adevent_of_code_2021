#! /usr/bin/env python3
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        grid = [list(line.rstrip()) for line in f]
    expanded_grid = expand(grid)
    locations = get_locations(expanded_grid)
    print(len(locations))
    distances = []
    distances_large = []
    for i, this_loc in enumerate(locations):
        other_locs = locations[(i + 1):]
        distances.append([])
        distances_large.append([])
        for loc in other_locs:
            dist = abs(this_loc[0] - loc[0]) + abs(this_loc[1] - loc[1])
            distances[i].append(dist)

            x0 = this_loc[1] if this_loc[1] <= loc[1] else loc[1]
            x1 = this_loc[1] if this_loc[1] > loc[1] else loc[1]
            y0 = this_loc[0] if this_loc[0] <= loc[0] else loc[0]
            y1 = this_loc[0] if this_loc[0] > loc[0] else loc[0]
            x_vals = expanded_grid[this_loc[0]][x0:x1+1]
            y_vals = [x[this_loc[1]] for x in expanded_grid[y0:y1+1]]
            add_x = x_vals.count("x") * 999998
            add_y = y_vals.count("x") * 999998
            dist_mil = abs(y0 - (y1 + add_y)) + abs(x0 - (x1 + add_x))
            distances_large[i].append(dist_mil)
    solution_1 = sum([sum(x) for x in distances])
    print(f"The solution for part 1 is {solution_1}")
    solution_2 = sum([sum(x) for x in distances_large])
    print(f"The solution for part 2 is {solution_2}")
    

def print_grid(grid):
    for row in grid:
        print(row)

def expand(grid):
    rows_to_insert = []
    cols_to_insert = []
    for i, row in enumerate(grid):
        rowset = set(row)
        if len(rowset) == 1 and rowset.pop() == ".":
            rows_to_insert.append(i)
    for i, _ in enumerate(grid[0]):
        this_col = [x[i] for x in grid]
        colset = set(this_col)
        if len(colset) == 1 and colset.pop() == ".":
            cols_to_insert.append(i)
    for idx in reversed(rows_to_insert):
        grid.insert(idx, ["x"] * len(grid[0]))
    for idx in reversed(cols_to_insert):
        for i, row in enumerate(grid):
            grid[i].insert(idx, "x")
    return grid

def get_locations(grid):
    locations = []
    for y, row in enumerate(grid):
        offset = 0
        while True:
            try:
                x = row.index("#", offset)
                locations.append((y, x))
                offset = x + 1
            except ValueError:
                break
    return locations

if __name__ == "__main__":
    main()
    