#! /usr/bin/env python3
from pathlib import Path
import re
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    solution_1 = num_and_sum(input_lines)
    print(f"The solution for part 1 is {solution_1}")

    lines_part2 = [text2num(line) for line in input_lines]
    solution_2 = num_and_sum(lines_part2)
    print(f"The solution for part 2 is {solution_2}")

def num_and_sum(input_list):
    input_nums = [re.sub(r"\D", "", line) for line in input_list]
    numbers = [int(f"{num[0]}{num[-1]}") for num in input_nums]
    solution = 0
    for x in numbers:
        solution += x
    return solution

def text2num(line):
    replacements = {
        r"(?<=o)n(?=e)": "1",
        r"(?<=t)w(?=o)": "2",
        r"(?<=t)hre(?=e)": "3",
        r"four": "4",
        r"fiv(?=e)": "5",
        r"six": "6",
        r"seve(?=n)": "7",
        r"(?<=e)igh(?=t)": "8",
        r"(?<=n)in(?=e)": "9"
    }
    for text, digit in replacements.items():
        line = re.sub(text, digit, line)
    return line


if __name__ == "__main__":
    main()