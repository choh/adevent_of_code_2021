use std::env;
use std::fs;
use std::process;
use std::collections::VecDeque;
use std::collections::HashSet;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let lines: Vec<&str> = file_content.lines().collect();
    let coordinates: Vec<Vec<usize>> = lines
        .into_iter()
        .map(|x| x.split(",").collect::<Vec<&str>>()
            .into_iter()
            .map(|y| y.parse::<usize>().unwrap())
            .collect()
        )
        .collect();
    let max_extent: usize = (*coordinates.iter()
            .map(|x| *x.iter().max().unwrap())
            .collect::<Vec<usize>>()
            .iter()
            .max()
            .unwrap()
        ).try_into().unwrap();
    let mut map: Vec<Vec<Vec<u8>>> = Vec::new();
    for x in 0..=max_extent {
        map.push(Vec::new());
        for y in 0..=max_extent {
            map[x].push(Vec::new());
            for _ in 0..=max_extent {
                map[x][y] = vec![0; max_extent + 1];
            }
        }
    }
    for coord in &coordinates {
        map[coord[0]][coord[1]][coord[2]] = 1;
    }
    let mut free_sides: u32 = 0;
    for row in &coordinates {
        free_sides += find_free_sides(
            &map, row[0], row[1], row[2], max_extent
        );
    }

    let outside_points = find_exterior_sides(&map, max_extent);
    let mut negative_map: Vec<Vec<Vec<u8>>> = Vec::new();
    for x in 0..=max_extent {
        negative_map.push(Vec::new());
        for y in 0..=max_extent {
            negative_map[x].push(Vec::new());
            for _ in 0..=max_extent {
                negative_map[x][y] = vec![1; max_extent + 1];
            }
        }
    }
    for coord in &outside_points {
        negative_map[coord.0][coord.1][coord.2] = 0;
    }
    let mut exterior_sides: u32 = 0;
    for row in &coordinates {
        exterior_sides += find_free_sides(
            &negative_map, row[0], row[1], row[2], max_extent
        );
    }

    println!("Solution 1 is {}", free_sides);
    println!("Solution 2 is {}", exterior_sides);

}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn find_free_sides(map: &Vec<Vec<Vec<u8>>>, 
                   x: usize, y: usize, z: usize, max: usize) -> u32 {
    let mut free_sides: u32 = 0;
    let neighbours = neighbours((x, y, z), max);
    for i in &neighbours {
        if map[i.0][i.1][i.2] == 0 {
            free_sides += 1;
        }
    }
    free_sides += 6 - neighbours.len() as u32;
    return free_sides;
}

fn find_exterior_sides(map: &Vec<Vec<Vec<u8>>>, max: usize) -> 
    HashSet<(usize, usize, usize)> {
    let start: (usize, usize, usize) = (0, 0, 0);
    let mut to_visit: VecDeque<(usize, usize, usize)> = VecDeque::new();
    let mut visited: HashSet<(usize, usize, usize)> = HashSet::new();
    let mut outside: HashSet<(usize, usize, usize)> = HashSet::new();
    to_visit.push_back(start);
    loop {
        let this_node = to_visit.pop_front().unwrap();
        if !visited.contains(&this_node) {
            visited.insert(this_node);
        }
        if map[this_node.0][this_node.1][this_node.2] == 0 {
            outside.insert(this_node);
            for next in neighbours(this_node, max) {
                if !visited.contains(&next) {
                    if map[next.0][next.1][next.2] == 0 { 
                        if !to_visit.contains(&next) {
                            to_visit.push_back(next);
                        }
                    } else {
                        visited.insert(next);
                    }
                }
            }
        }
        if to_visit.len() == 0 {
            break;
        }
    }
    return outside;
}

fn neighbours(coord: (usize, usize, usize), 
              max: usize) -> Vec<(usize, usize, usize)> {
    let mut neighbours: Vec<(usize, usize, usize)> = Vec::new();
    if coord.0 != 0 {
        neighbours.push((coord.0 - 1, coord.1, coord.2))
    }
    if coord.0 != max {
        neighbours.push((coord.0 + 1, coord.1, coord.2))
    }
    if coord.1 != 0 {
        neighbours.push((coord.0, coord.1 - 1, coord.2))
    }
    if coord.1 != max {
        neighbours.push((coord.0, coord.1 + 1, coord.2))
    }
    if coord.2 != 0 {
        neighbours.push((coord.0, coord.1, coord.2 - 1))
    }
    if coord.2 != max {
        neighbours.push((coord.0, coord.1, coord.2 + 1))
    }
    return neighbours;
}