use std::env;
use std::fs;
use std::process;
use std::collections::HashSet;

#[derive(Clone, Copy)]
struct Coordinate {
    hx: i32,
    hy: i32,
    tx: i32,
    ty: i32
}

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let moves: Vec<&str> = file_content.split("\n").collect();
    let mut pos = Coordinate {
        hx: 0, 
        hy: 0,
        tx: 0,
        ty: 0
    };
    let mut tail_positions: HashSet<[i32; 2]> = HashSet::new();
    tail_positions.insert([pos.tx, pos.ty]);

    for this_move in &moves {
        let move_def:Vec<&str> = this_move.split(" ").collect();
        let direction = move_def[0];
        let distance = move_def[1].parse::<usize>().unwrap();
        for _ in 0..distance {
            pos = update_head(pos, direction);
            pos = update_tail(pos);
            tail_positions.insert([pos.tx, pos.ty]);
        }
    }
    let tail_pos_count = tail_positions.len();
    println!("Solution 1 is {}", tail_pos_count);

    let mut many_knots: Vec<Coordinate> = Vec::new();
    let number_of_knots = 9;
    for _ in 0..number_of_knots {
        let this_coord = Coordinate {
            hx: 0, 
            hy: 0,
            tx: 0,
            ty: 0
        };
        many_knots.push(this_coord);
    }

    let mut tail_positions_long: HashSet<[i32; 2]> = HashSet::new();
    let last_idx = number_of_knots - 1;
    tail_positions_long.insert([many_knots[last_idx].tx, 
                                many_knots[last_idx].ty]);
    
    for this_move in &moves {
        let move_def:Vec<&str> = this_move.split(" ").collect();
        let direction = move_def[0];
        let distance = move_def[1].parse::<usize>().unwrap();
        for _ in 0..distance {
            many_knots[0] = update_head(many_knots[0], direction);
            many_knots[0] = update_tail(many_knots[0]);
            for i in 1..number_of_knots {
                many_knots[i].hx = many_knots[i - 1].tx;
                many_knots[i].hy = many_knots[i - 1].ty;
                many_knots[i] = update_tail(many_knots[i]);
            }
            tail_positions_long.insert([many_knots[last_idx].tx, 
                                        many_knots[last_idx].ty]);
        }       
    }
    let tail_pos_count_2 = tail_positions_long.len();
    println!("Solution 2 is {}", tail_pos_count_2);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn update_head(coord: Coordinate, move_str: &str) -> Coordinate {
    let new_hx: i32;
    let new_hy: i32;

    if move_str == "R" {
        new_hx = coord.hx + 1;
        new_hy = coord.hy;
    } else if move_str == "L" {
        new_hx = coord.hx - 1;
        new_hy = coord.hy;
    } else if move_str == "U" {
        new_hx = coord.hx;
        new_hy = coord.hy + 1;
    } else if move_str == "D" {
        new_hx = coord.hx;
        new_hy = coord.hy - 1;
    } else {
        println!("Unknown direction {}", move_str);
        process::exit(2);
    }

    return Coordinate {
        hx: new_hx,
        hy: new_hy,
        tx: coord.tx,
        ty: coord.ty
    };
}

fn update_tail(coord: Coordinate) -> Coordinate {
    let new_tx: i32;
    let new_ty: i32;

    let dist_tx: i32 = coord.hx - coord.tx;
    let dist_ty: i32 = coord.hy - coord.ty;

    let movement_x = if dist_tx > 0 { 1 } else { -1 };
    let movement_y = if dist_ty > 0 { 1 } else { -1 };

    if dist_tx.abs() > 1 && dist_ty == 0  {
        new_tx = coord.tx + movement_x;
        new_ty = coord.ty;
    } else if dist_tx == 0 && dist_ty.abs() > 1 {
        new_ty = coord.ty + movement_y;
        new_tx = coord.tx;
    } else if (dist_tx.abs() > 1 && dist_ty.abs() >= 1) || 
              (dist_tx.abs() >= 1 && dist_ty.abs() > 1) {
        new_tx = coord.tx + movement_x;
        new_ty = coord.ty + movement_y;
    } else {
        new_tx = coord.tx;
        new_ty = coord.ty;
    }

    return Coordinate {
        hx: coord.hx, 
        hy: coord.hy,
        tx: new_tx,
        ty: new_ty
    };
}