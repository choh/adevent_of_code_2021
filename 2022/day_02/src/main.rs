use std::env;
use std::fs;
use std::process;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let game_moves = split_moves(&file_content);
    let result_1 = score_game(&game_moves, 1);
    println!("Solution 1 is {}.", result_1);
    let result_2 = score_game(&game_moves, 2);
    println!("Solution 2 is {}.", result_2);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn split_moves(game_file_content: &String) -> Vec<&str> {
    let lines: Vec<&str> = game_file_content.split("\n").collect();
    return lines;
}

fn score_game(game_moves: &Vec<&str>, rules: i32) -> i32 {
    let mut game_sum: i32 = 0;
    for this_move in game_moves {
        let round: Vec<&str> = this_move.split(" ").collect();
        if rules == 1 {
            game_sum += score_rules_1(&round);
        } else if rules == 2 {
            game_sum += score_rules_2(&round);
        }
        
    }
    return game_sum;
}

fn score_rules_1(moves_vec: &Vec<&str>) -> i32 {
    // A, B, C -> rock, paper, scissors
    // X, Y, Z -> rock, paper, scissors
    let move_score: i32 = match moves_vec[1] {
        "X" => 1,
        "Y" => 2,
        "Z" => 3,
        _ => -1
    };
    let mut result_score: i32 = 0;
    if moves_vec[0] == "A" {
      result_score = match moves_vec[1] {
        "X" => 3,
        "Y" => 6,
        "Z" => 0,
        _ => -1
      };
    } else if moves_vec[0] == "B" {
        result_score = match moves_vec[1] {
            "X" => 0,
            "Y" => 3,
            "Z" => 6,
            _ => -1
        };
    } else if moves_vec[0] == "C" {
        result_score = match moves_vec[1] {
            "X" => 6,
            "Y" => 0,
            "Z" => 3,
            _ => -1
        };
    }
    return result_score + move_score;
}

fn score_rules_2(moves_vec: &Vec<&str>) -> i32 {
    // A, B, C -> rock, paper, scissors
    // X, Y, Z -> lose, draw, win
    let result_score: i32 = match moves_vec[1] {
        "X" => 0,
        "Y" => 3,
        "Z" => 6,
        _ => -1
    };
    let mut move_score: i32 = 0;
    if moves_vec[0] == "A" {
        move_score = match moves_vec[1] {
            "X" => 3,
            "Y" => 1,
            "Z" => 2,
            _ => -1
        };
    } else if moves_vec[0] == "B" {
        move_score = match moves_vec[1] {
            "X" => 1,
            "Y" => 2,
            "Z" => 3,
            _ => -1
        };
    } else if moves_vec[0] == "C" {
        move_score = match moves_vec[1] {
            "X" => 2,
            "Y" => 3,
            "Z" => 1,
            _ => -1
        };
    }
    return result_score + move_score;
}