use std::env;
use std::fs;
use std::process;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let instructions: Vec<&str> = file_content.split("\n").collect();

    let mut register_x: i32 = 1;
    let mut cycles = 0;
    let mut intermediate_x: Vec<i32> = Vec::new();
    let mut screen: Vec<Vec<char>> = Vec::new();
    for _ in 0..6 {
        screen.push(vec![' '; 40]);
    }
    intermediate_x.push(0);

    for this_inst in instructions {
        let tokens: Vec<&str> = this_inst.split(" ").collect();
        let wait_timer = match tokens[0] {
            "noop" => 1,
            "addx" => 2,
            _ => 0
        };

        for _ in 0..wait_timer {
            cycles += 1;
            let screen_row = ((cycles - 1)  / 40) as usize;   
            let scr_idx = (cycles - 1 - screen_row as i32 * 40) as usize;
            if screen_row > 5 {
                break;
            }
            if scr_idx as i32 == register_x + 1 || 
               scr_idx as i32 == register_x - 1 || 
               scr_idx as i32 == register_x {
                screen[screen_row][scr_idx] = '#';
            }
            if (cycles + 20) % 40 == 0 {
                intermediate_x.push(register_x * cycles);
            }
        }
        if tokens[0] == "addx" {
            let val = tokens[1].parse::<i32>().unwrap();
            register_x += val;
        }
    }
    let solution_1: i32 = intermediate_x.iter().sum();
    println!("Solution 1 is {}", solution_1);
    println!("Solution 2 is:");
    for row in screen {
        let print_scr: String = row.into_iter().collect();
        println!("{}", print_scr);
    }
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}