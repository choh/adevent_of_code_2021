use std::env;
use std::fs;
use std::process;

struct MoveDef {
    amount: u32,
    from: usize,
    to: usize
}

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let split_content: Vec<&str> = file_content.split("\n").collect();
    let mut stack_vec: Vec<usize> = Vec::new();
    let mut value_vec: Vec<char> = Vec::new();
    let mut moves_vec: Vec<MoveDef> = Vec::new();
    for line in split_content {
        if line.contains("[") {
            let (stack, value) = parse_boxes(&line);
            for item in stack {
                stack_vec.push(item);
            }
            for item in value {
                value_vec.push(item);
            }
        } else if line.contains("move") {
            let this_move = make_movedef(&line);
            moves_vec.push(this_move);
        }
    }
    let mut boxes: Vec<Vec<char>> = Vec::new();
    let stack_count = *stack_vec.iter().max().unwrap();
    for _ in 0..stack_count + 1 {
        boxes.push(Vec::new());
    }
    for i in (0..value_vec.len()).rev() {
        boxes[stack_vec[i]].push(value_vec[i]);
    }
    let moved_boxes_1 = make_moves(&boxes, &moves_vec);
    let top_boxes_1 = get_word(moved_boxes_1);
    println!("Solution 1 is {}", top_boxes_1);
    let moved_boxes_2 = make_moves_advanced(&boxes, &moves_vec);
    let top_boxes_2 = get_word(moved_boxes_2);
    println!("Solution 2 is {}", top_boxes_2);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn parse_boxes(box_def: &str) -> (Vec<usize>, Vec<char>) {
    let mut positions: Vec<usize> = Vec::new();
    let mut content: Vec<char> = Vec::new();
    let box_vec: Vec<char> = box_def.chars().collect();
    let line_len = box_vec.len();
    for i in (1..line_len).step_by(4) {
        let vec_pos: usize = (i as i32 / 4).try_into().unwrap();
        if box_vec[i].is_alphabetic() {
            positions.push(vec_pos);
            content.push(box_vec[i]);
        }  
    }
    return (positions, content);
}

fn make_movedef(move_line: &str) -> MoveDef {
    let mut use_values: Vec<u32> = Vec::new();
    for token in move_line.split(" ") {
        let asnum = token.parse::<u32>();
        match asnum {
            Ok(val) => use_values.push(val),
            Err(_) => continue,
        }
    }
    return MoveDef {
        amount: use_values[0],
        from: (use_values[1] - 1).try_into().unwrap(),
        to: (use_values[2] - 1).try_into().unwrap()
    }
}

fn make_moves(boxes: &Vec<Vec<char>>, 
              moves: &Vec<MoveDef>) -> Vec<Vec<char>> {
    let mut local_boxes = boxes.clone();
    for this_move in moves {
        for _ in 0..this_move.amount {
            let move_item = local_boxes[this_move.from].pop().unwrap();
            local_boxes[this_move.to].push(move_item);
        }
    }
    return local_boxes;
}

fn make_moves_advanced(boxes: &Vec<Vec<char>>, 
                       moves: &Vec<MoveDef>) -> Vec<Vec<char>> {
    let mut local_boxes = boxes.clone();
    for this_move in moves {
        let mut to_move: Vec<char> = Vec::new();
        for _ in 0..this_move.amount {
            let move_item = local_boxes[this_move.from].pop().unwrap();
            to_move.push(move_item);
        }
        to_move.reverse();
        for item in to_move {
            local_boxes[this_move.to].push(item);
        }
    }
    return local_boxes;
}

fn get_word(boxes: Vec<Vec<char>>) -> String {
    let mut chars: Vec<char> = Vec::new();
    for stack in boxes {
        let stack_size = stack.len();
        if stack_size > 0 {
            chars.push(stack[stack_size - 1]);
        }
    }
    return chars.into_iter().collect();
}