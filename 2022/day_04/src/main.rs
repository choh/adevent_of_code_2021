use std::env;
use std::fs;
use std::process;
use std::collections::HashSet;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let pairs: Vec<&str> = file_content.split("\n").collect();
    let mut fully_contained = 0;
    let mut has_overlap = 0;
    for this_pair in pairs {
        let set_pair = to_set_vec(&this_pair);
        fully_contained += is_fully_contained(&set_pair);
        has_overlap += is_overlap(&set_pair);
    }
    println!("Solution 1 is {}", fully_contained);
    println!("Solution 2 is {}", has_overlap);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn to_set_vec(pair_str: &str) -> Vec<HashSet<u32>> {
    let work_vec: Vec<&str> = pair_str.split(",").collect();
    let mut work_sections: Vec<HashSet<u32>> = Vec::new();
    for this_work in work_vec {
        let bounds: Vec<&str> = this_work.split("-").collect();
        let start: u32 = bounds[0].parse().unwrap();
        let end: u32 = bounds[1].parse().unwrap();
        let mut sections: HashSet<u32> = HashSet::new();
        for i in start..end + 1 {
            sections.insert(i);
        }
        work_sections.push(sections);
    }
    return work_sections;
}

fn is_fully_contained(work_sections: &Vec<HashSet<u32>>) -> u32 {
    let mut result = 0;
    if work_sections[0].is_subset(&work_sections[1]) || 
       work_sections[1].is_subset(&work_sections[0]) {
        result = 1;
    }
    return result;
}

fn is_overlap(work_sections: &Vec<HashSet<u32>>) -> u32 {
    let values_in_both = work_sections[0].len() + work_sections[1].len();
    let combined_set: HashSet<_> = work_sections[0]
        .union(&work_sections[1])
        .collect();
    let mut result = 0;
    if values_in_both > combined_set.len() {
        result = 1;
    }
    return result;
}