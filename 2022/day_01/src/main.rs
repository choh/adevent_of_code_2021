use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::env;
use std::str::FromStr;

fn main() {
    let args: Vec<String> = env::args().collect();
    let read_file_name = &args[1];
    let mut calories: Vec<i32> = Vec::new(); 
    let mut all_calories: Vec<Vec<i32>> = Vec::new();

    if let Ok(lines) = read_file(read_file_name) {
        for line in lines {
            if let Ok(this_line) = line {
                let line_as_int = i32::from_str(&this_line).unwrap_or(-1);
                if line_as_int == -1 {
                    all_calories.push(calories);
                    calories = Vec::new();
                    continue;
                }
                calories.push(line_as_int);      
            }
        }
        // need to handle last value somehow
        all_calories.push(calories);
    }

    let calories_per_elf  = count_calories(all_calories);
    max_cal(&calories_per_elf);
    top_3_calories(&calories_per_elf);
}

fn read_file<T>(file_path: T) -> io::Result<io::Lines<io::BufReader<File>>> where T: AsRef<Path>, {
    let file = File::open(file_path)?;
    Ok(io::BufReader::new(file).lines())
}

fn count_calories(calorie_vector: Vec<Vec<i32>>) -> Vec<i32> {
    let mut calorie_sums: Vec<i32> = Vec::new();
    for item in calorie_vector {
        let mut cal_sum: i32 = 0;
        for cal in item {
            cal_sum = cal_sum + cal;
        }
        calorie_sums.push(cal_sum)
    }
    return calorie_sums;
}

fn max_cal(calorie_vector: &Vec<i32>) -> () {
    let result_1 = calorie_vector.iter().max().unwrap();
    println!("Solution 1 is {}", result_1);
}

fn top_3_calories(calorie_vector: &Vec<i32>) -> () {
    let mut calorie_vector = calorie_vector.clone();
    calorie_vector.sort_by(|a, b| b.cmp(a));
    let mut top_calorie_sum: i32 = 0;
    for i in 0..3 {
        top_calorie_sum += calorie_vector[i];
    }
    println!("Solution 2 is {}", top_calorie_sum);
}