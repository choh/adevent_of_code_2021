use std::env;
use std::fs;
use std::process;
use std::collections::HashSet;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let signal_start = get_start(&file_content, 4);
    println!("Solution 1 is {}", signal_start);
    let msg_start = get_start(&file_content, 14);
    println!("Solution 2 is {}", msg_start);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn get_start(msg: &str, after: usize) -> usize {
    let msg_chars: Vec<char> = msg.chars().collect();
    let maxlen = msg_chars.len();
    let mut start: usize = 0;
    for i in (after - 1)..maxlen {
        let mut part: HashSet<&char> = HashSet::new();
        for x in &msg_chars[i-(after - 1)..=i] {
            part.insert(x);
        }
        if part.len() == after {
            start = i + 1;
            break;
        }
    }
    return start;
}