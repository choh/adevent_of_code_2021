use std::env;
use std::fs;
use std::process;

struct Rucksack {
    side1: Vec<char>,
    side2: Vec<char>
}

struct ElfGroup {
    elf1: Vec<char>,
    elf2: Vec<char>,
    elf3: Vec<char>
}

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);

    // part 1
    let rucksack_sides = split_data(&file_content);
    let mut first_matches: Vec<char> = Vec::new();
    for side in rucksack_sides {
        let this_match = common_char(&side.side1, &side.side2);
        first_matches.push(this_match);
    }
    let solution_1 = score_matches(first_matches);
    println!("Solution 1 is {}", solution_1);

    // part 2
    let groups = get_elf_groups(&file_content);
    let mut common_items: Vec<char> = Vec::new();
    for group in groups {
        let group_match = find_group_match(&group);
        common_items.push(group_match);
    }
    let solution_2 = score_matches(common_items);
    println!("Solution 2 is {}", solution_2);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn split_data(data_string: &String) -> Vec<Rucksack> {
    let split_lines: Vec<&str> = data_string.split("\n").collect();
    let mut inner_split: Vec<Rucksack> = Vec::new();
    for line in split_lines {
        let halves = split_str_half(&line);
        inner_split.push(halves);
    }
    return inner_split;
}

fn split_str_half(in_str: &str) -> Rucksack {
    let char_count = in_str.chars().count();
    let split_pos = char_count / 2;
    let mut i = 0;
    let mut char_vec_first: Vec<char> = Vec::new();
    let mut char_vec_second: Vec<char> = Vec::new();
    for chr in in_str.chars() {
        if i < split_pos {
            char_vec_first.push(chr);
        } else {
            char_vec_second.push(chr);
        }
        i = i + 1;
    }
    return Rucksack {
        side1: char_vec_first, 
        side2: char_vec_second
    };
}

fn common_char(v1: &Vec<char>, v2: &Vec<char>) -> char {
    let mut match_item: Option<char> = None;
    for chr in v1 {
        if v2.contains(&chr) {
            match_item = Some(*chr);
            break;
        }
    }
    let found_match = match match_item {
        Some(match_item) => match_item,
        None => {
            println!("Could not find match.");
            process::exit(2);
        }
    };
    return found_match;
}

fn score_matches(values: Vec<char>) -> u32 {
    let mut total_score: u32 = 0;
    for item in values {
        let numval = item as u32;
        if item.is_ascii_lowercase() {
            // shift to 1
            total_score += numval - 96
        } else {
            // shift to 1 and then to the back of the lower case alphabet
            total_score += numval - 64 + 26;
        }
    }
    return total_score;
}

fn get_elf_groups(file_content: &String) -> Vec<ElfGroup> {
    let individuals: Vec<&str> = file_content.split("\n").collect();
    let mut elf_groups: Vec<ElfGroup> = Vec::new();
    let groups = individuals.len() / 3;
    for i in 0..groups {
        elf_groups.push(
            ElfGroup {
                elf1: individuals[i * 3].chars().collect(),
                elf2: individuals[i * 3 + 1].chars().collect(),
                elf3: individuals[i * 3 + 2].chars().collect()
            }
        )
    }
    return elf_groups;
}

fn find_group_match(this_group: &ElfGroup) -> char { 
    let mut matches_12: Vec<char> = Vec::new();
    let mut matches_13: Vec<char> = Vec::new();
    for chr in &this_group.elf1 {
        if this_group.elf2.contains(&chr) {
            matches_12.push(*chr);
        }
        if this_group.elf3.contains(&chr) {
            matches_13.push(*chr);
        }
    }
    let found_match = common_char(&matches_12, &matches_13);
    return found_match;
}