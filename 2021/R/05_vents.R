library("dplyr")
library("tidyr")

coords_raw <- readr::read_lines(here::here("data-raw", "05_input.txt"))
coords_raw <- gsub(" -> ", ",", coords_raw)

coords_tbl <- data.frame(raw = coords_raw)
coords_tbl <- coords_tbl |>
    separate(raw, into = c("x1", "y1", "x2", "y2"), sep = ",")

hv_lines <- coords_tbl |>
    filter(x1 == x2 | y1 == y2)

intermediate_points <- function(x1, y1, x2, y2) {
    xcor <- x1:x2
    ycor <- y1:y2
    data.frame(xcor, ycor)
}

find_overlaps <- function(lines_data) {
    line_coord <- apply(lines_data, 1, function(x)
        intermediate_points(x[1], x[2], x[3], x[4])) |>
        listr::list_bind(everything()) |>
        listr::list_extract(1)

    line_coord <- line_coord |>
        group_by(xcor, ycor) |>
        summarise(n = n(), .groups = "drop")

    line_coord |>
        filter(n > 1) |>
        nrow()
}

solution1 <- find_overlaps(hv_lines)
solution2 <- find_overlaps(coords_tbl)
